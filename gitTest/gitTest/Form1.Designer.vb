﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDiceCalc
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDiceCalc))
        Me.btnRollD4 = New System.Windows.Forms.Button()
        Me.btnRollD6 = New System.Windows.Forms.Button()
        Me.btnRollD8 = New System.Windows.Forms.Button()
        Me.btnRollD10 = New System.Windows.Forms.Button()
        Me.btnRollD12 = New System.Windows.Forms.Button()
        Me.btnRollD20 = New System.Windows.Forms.Button()
        Me.btnRollPercent = New System.Windows.Forms.Button()
        Me.rtbDebug = New System.Windows.Forms.RichTextBox()
        Me.lblResultLog = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblResult = New System.Windows.Forms.Label()
        Me.picD41 = New System.Windows.Forms.PictureBox()
        Me.picD42 = New System.Windows.Forms.PictureBox()
        Me.picD43 = New System.Windows.Forms.PictureBox()
        Me.picD44 = New System.Windows.Forms.PictureBox()
        Me.picD61 = New System.Windows.Forms.PictureBox()
        Me.picD62 = New System.Windows.Forms.PictureBox()
        Me.picD63 = New System.Windows.Forms.PictureBox()
        Me.picD64 = New System.Windows.Forms.PictureBox()
        Me.picD65 = New System.Windows.Forms.PictureBox()
        Me.picD66 = New System.Windows.Forms.PictureBox()
        Me.picD81 = New System.Windows.Forms.PictureBox()
        Me.picD82 = New System.Windows.Forms.PictureBox()
        Me.picD83 = New System.Windows.Forms.PictureBox()
        Me.picD84 = New System.Windows.Forms.PictureBox()
        Me.picD85 = New System.Windows.Forms.PictureBox()
        Me.picD86 = New System.Windows.Forms.PictureBox()
        Me.picD87 = New System.Windows.Forms.PictureBox()
        Me.picD88 = New System.Windows.Forms.PictureBox()
        Me.picD100 = New System.Windows.Forms.PictureBox()
        Me.picD101 = New System.Windows.Forms.PictureBox()
        Me.picD102 = New System.Windows.Forms.PictureBox()
        Me.picD103 = New System.Windows.Forms.PictureBox()
        Me.picD104 = New System.Windows.Forms.PictureBox()
        Me.picD105 = New System.Windows.Forms.PictureBox()
        Me.picD106 = New System.Windows.Forms.PictureBox()
        Me.picD107 = New System.Windows.Forms.PictureBox()
        Me.picD108 = New System.Windows.Forms.PictureBox()
        Me.picD109 = New System.Windows.Forms.PictureBox()
        Me.picD1201 = New System.Windows.Forms.PictureBox()
        Me.picD2020 = New System.Windows.Forms.PictureBox()
        Me.picPercent1000 = New System.Windows.Forms.PictureBox()
        Me.picPercent1010 = New System.Windows.Forms.PictureBox()
        Me.picPercent1020 = New System.Windows.Forms.PictureBox()
        Me.picPercent1030 = New System.Windows.Forms.PictureBox()
        Me.picPercent1040 = New System.Windows.Forms.PictureBox()
        Me.picPercent1050 = New System.Windows.Forms.PictureBox()
        Me.picPercent1060 = New System.Windows.Forms.PictureBox()
        Me.picPercent1070 = New System.Windows.Forms.PictureBox()
        Me.picPercent1080 = New System.Windows.Forms.PictureBox()
        Me.picPercent1090 = New System.Windows.Forms.PictureBox()
        Me.picPercent10 = New System.Windows.Forms.PictureBox()
        Me.picPercent11 = New System.Windows.Forms.PictureBox()
        Me.picPercent12 = New System.Windows.Forms.PictureBox()
        Me.picPercent13 = New System.Windows.Forms.PictureBox()
        Me.picPercent14 = New System.Windows.Forms.PictureBox()
        Me.picPercent15 = New System.Windows.Forms.PictureBox()
        Me.picPercent16 = New System.Windows.Forms.PictureBox()
        Me.picPercent17 = New System.Windows.Forms.PictureBox()
        Me.picPerecent18 = New System.Windows.Forms.PictureBox()
        Me.picPercent19 = New System.Windows.Forms.PictureBox()
        Me.txtCalcAlgorithm = New System.Windows.Forms.TextBox()
        Me.btnCustomCalc = New System.Windows.Forms.Button()
        CType(Me.picD41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD102, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD105, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD106, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD108, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD109, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD1201, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picD2020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent1000, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent1010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent1020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent1030, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent1040, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent1050, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent1060, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent1070, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent1080, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent1090, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPerecent18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPercent19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnRollD4
        '
        Me.btnRollD4.Location = New System.Drawing.Point(12, 81)
        Me.btnRollD4.Name = "btnRollD4"
        Me.btnRollD4.Size = New System.Drawing.Size(75, 23)
        Me.btnRollD4.TabIndex = 0
        Me.btnRollD4.Text = "D4"
        Me.btnRollD4.UseVisualStyleBackColor = True
        '
        'btnRollD6
        '
        Me.btnRollD6.Location = New System.Drawing.Point(12, 179)
        Me.btnRollD6.Name = "btnRollD6"
        Me.btnRollD6.Size = New System.Drawing.Size(75, 23)
        Me.btnRollD6.TabIndex = 1
        Me.btnRollD6.Text = "D6"
        Me.btnRollD6.UseVisualStyleBackColor = True
        '
        'btnRollD8
        '
        Me.btnRollD8.Location = New System.Drawing.Point(12, 277)
        Me.btnRollD8.Name = "btnRollD8"
        Me.btnRollD8.Size = New System.Drawing.Size(75, 23)
        Me.btnRollD8.TabIndex = 2
        Me.btnRollD8.Text = "D8"
        Me.btnRollD8.UseVisualStyleBackColor = True
        '
        'btnRollD10
        '
        Me.btnRollD10.Location = New System.Drawing.Point(142, 81)
        Me.btnRollD10.Name = "btnRollD10"
        Me.btnRollD10.Size = New System.Drawing.Size(75, 23)
        Me.btnRollD10.TabIndex = 3
        Me.btnRollD10.Text = "D10"
        Me.btnRollD10.UseVisualStyleBackColor = True
        '
        'btnRollD12
        '
        Me.btnRollD12.Location = New System.Drawing.Point(142, 179)
        Me.btnRollD12.Name = "btnRollD12"
        Me.btnRollD12.Size = New System.Drawing.Size(75, 23)
        Me.btnRollD12.TabIndex = 4
        Me.btnRollD12.Text = "D12"
        Me.btnRollD12.UseVisualStyleBackColor = True
        '
        'btnRollD20
        '
        Me.btnRollD20.Location = New System.Drawing.Point(142, 277)
        Me.btnRollD20.Name = "btnRollD20"
        Me.btnRollD20.Size = New System.Drawing.Size(75, 23)
        Me.btnRollD20.TabIndex = 5
        Me.btnRollD20.Text = "D20"
        Me.btnRollD20.UseVisualStyleBackColor = True
        '
        'btnRollPercent
        '
        Me.btnRollPercent.Location = New System.Drawing.Point(262, 81)
        Me.btnRollPercent.Name = "btnRollPercent"
        Me.btnRollPercent.Size = New System.Drawing.Size(75, 23)
        Me.btnRollPercent.TabIndex = 7
        Me.btnRollPercent.Text = "Percent Die"
        Me.btnRollPercent.UseVisualStyleBackColor = True
        '
        'rtbDebug
        '
        Me.rtbDebug.Location = New System.Drawing.Point(530, 107)
        Me.rtbDebug.Name = "rtbDebug"
        Me.rtbDebug.Size = New System.Drawing.Size(166, 189)
        Me.rtbDebug.TabIndex = 8
        Me.rtbDebug.Text = ""
        '
        'lblResultLog
        '
        Me.lblResultLog.Location = New System.Drawing.Point(265, 107)
        Me.lblResultLog.Name = "lblResultLog"
        Me.lblResultLog.Size = New System.Drawing.Size(255, 137)
        Me.lblResultLog.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(269, 258)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Total:"
        '
        'lblResult
        '
        Me.lblResult.Location = New System.Drawing.Point(309, 244)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(211, 55)
        Me.lblResult.TabIndex = 11
        '
        'picD41
        '
        Me.picD41.Image = CType(resources.GetObject("picD41.Image"), System.Drawing.Image)
        Me.picD41.Location = New System.Drawing.Point(12, 12)
        Me.picD41.Name = "picD41"
        Me.picD41.Size = New System.Drawing.Size(75, 63)
        Me.picD41.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD41.TabIndex = 12
        Me.picD41.TabStop = False
        '
        'picD42
        '
        Me.picD42.Image = CType(resources.GetObject("picD42.Image"), System.Drawing.Image)
        Me.picD42.Location = New System.Drawing.Point(12, 12)
        Me.picD42.Name = "picD42"
        Me.picD42.Size = New System.Drawing.Size(75, 63)
        Me.picD42.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD42.TabIndex = 13
        Me.picD42.TabStop = False
        '
        'picD43
        '
        Me.picD43.Image = CType(resources.GetObject("picD43.Image"), System.Drawing.Image)
        Me.picD43.Location = New System.Drawing.Point(12, 12)
        Me.picD43.Name = "picD43"
        Me.picD43.Size = New System.Drawing.Size(75, 63)
        Me.picD43.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD43.TabIndex = 14
        Me.picD43.TabStop = False
        '
        'picD44
        '
        Me.picD44.Image = CType(resources.GetObject("picD44.Image"), System.Drawing.Image)
        Me.picD44.Location = New System.Drawing.Point(12, 12)
        Me.picD44.Name = "picD44"
        Me.picD44.Size = New System.Drawing.Size(75, 63)
        Me.picD44.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD44.TabIndex = 15
        Me.picD44.TabStop = False
        '
        'picD61
        '
        Me.picD61.Image = CType(resources.GetObject("picD61.Image"), System.Drawing.Image)
        Me.picD61.Location = New System.Drawing.Point(12, 110)
        Me.picD61.Name = "picD61"
        Me.picD61.Size = New System.Drawing.Size(75, 63)
        Me.picD61.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD61.TabIndex = 16
        Me.picD61.TabStop = False
        '
        'picD62
        '
        Me.picD62.Image = CType(resources.GetObject("picD62.Image"), System.Drawing.Image)
        Me.picD62.Location = New System.Drawing.Point(12, 110)
        Me.picD62.Name = "picD62"
        Me.picD62.Size = New System.Drawing.Size(75, 63)
        Me.picD62.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD62.TabIndex = 17
        Me.picD62.TabStop = False
        '
        'picD63
        '
        Me.picD63.Image = CType(resources.GetObject("picD63.Image"), System.Drawing.Image)
        Me.picD63.Location = New System.Drawing.Point(12, 110)
        Me.picD63.Name = "picD63"
        Me.picD63.Size = New System.Drawing.Size(75, 63)
        Me.picD63.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD63.TabIndex = 18
        Me.picD63.TabStop = False
        '
        'picD64
        '
        Me.picD64.Image = CType(resources.GetObject("picD64.Image"), System.Drawing.Image)
        Me.picD64.Location = New System.Drawing.Point(12, 110)
        Me.picD64.Name = "picD64"
        Me.picD64.Size = New System.Drawing.Size(75, 63)
        Me.picD64.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD64.TabIndex = 19
        Me.picD64.TabStop = False
        '
        'picD65
        '
        Me.picD65.Image = CType(resources.GetObject("picD65.Image"), System.Drawing.Image)
        Me.picD65.Location = New System.Drawing.Point(12, 110)
        Me.picD65.Name = "picD65"
        Me.picD65.Size = New System.Drawing.Size(75, 63)
        Me.picD65.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD65.TabIndex = 20
        Me.picD65.TabStop = False
        '
        'picD66
        '
        Me.picD66.Image = CType(resources.GetObject("picD66.Image"), System.Drawing.Image)
        Me.picD66.Location = New System.Drawing.Point(12, 110)
        Me.picD66.Name = "picD66"
        Me.picD66.Size = New System.Drawing.Size(75, 63)
        Me.picD66.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD66.TabIndex = 21
        Me.picD66.TabStop = False
        '
        'picD81
        '
        Me.picD81.Image = CType(resources.GetObject("picD81.Image"), System.Drawing.Image)
        Me.picD81.Location = New System.Drawing.Point(12, 208)
        Me.picD81.Name = "picD81"
        Me.picD81.Size = New System.Drawing.Size(75, 63)
        Me.picD81.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD81.TabIndex = 22
        Me.picD81.TabStop = False
        '
        'picD82
        '
        Me.picD82.Image = CType(resources.GetObject("picD82.Image"), System.Drawing.Image)
        Me.picD82.Location = New System.Drawing.Point(12, 208)
        Me.picD82.Name = "picD82"
        Me.picD82.Size = New System.Drawing.Size(75, 63)
        Me.picD82.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD82.TabIndex = 23
        Me.picD82.TabStop = False
        '
        'picD83
        '
        Me.picD83.Image = CType(resources.GetObject("picD83.Image"), System.Drawing.Image)
        Me.picD83.Location = New System.Drawing.Point(12, 208)
        Me.picD83.Name = "picD83"
        Me.picD83.Size = New System.Drawing.Size(75, 63)
        Me.picD83.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD83.TabIndex = 24
        Me.picD83.TabStop = False
        '
        'picD84
        '
        Me.picD84.Image = CType(resources.GetObject("picD84.Image"), System.Drawing.Image)
        Me.picD84.Location = New System.Drawing.Point(12, 208)
        Me.picD84.Name = "picD84"
        Me.picD84.Size = New System.Drawing.Size(75, 63)
        Me.picD84.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD84.TabIndex = 25
        Me.picD84.TabStop = False
        '
        'picD85
        '
        Me.picD85.Image = CType(resources.GetObject("picD85.Image"), System.Drawing.Image)
        Me.picD85.Location = New System.Drawing.Point(12, 208)
        Me.picD85.Name = "picD85"
        Me.picD85.Size = New System.Drawing.Size(75, 63)
        Me.picD85.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD85.TabIndex = 26
        Me.picD85.TabStop = False
        '
        'picD86
        '
        Me.picD86.Image = CType(resources.GetObject("picD86.Image"), System.Drawing.Image)
        Me.picD86.Location = New System.Drawing.Point(12, 208)
        Me.picD86.Name = "picD86"
        Me.picD86.Size = New System.Drawing.Size(75, 63)
        Me.picD86.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD86.TabIndex = 27
        Me.picD86.TabStop = False
        '
        'picD87
        '
        Me.picD87.Image = CType(resources.GetObject("picD87.Image"), System.Drawing.Image)
        Me.picD87.Location = New System.Drawing.Point(12, 208)
        Me.picD87.Name = "picD87"
        Me.picD87.Size = New System.Drawing.Size(75, 63)
        Me.picD87.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD87.TabIndex = 28
        Me.picD87.TabStop = False
        '
        'picD88
        '
        Me.picD88.Image = CType(resources.GetObject("picD88.Image"), System.Drawing.Image)
        Me.picD88.Location = New System.Drawing.Point(12, 208)
        Me.picD88.Name = "picD88"
        Me.picD88.Size = New System.Drawing.Size(75, 63)
        Me.picD88.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD88.TabIndex = 29
        Me.picD88.TabStop = False
        '
        'picD100
        '
        Me.picD100.Image = CType(resources.GetObject("picD100.Image"), System.Drawing.Image)
        Me.picD100.Location = New System.Drawing.Point(142, 12)
        Me.picD100.Name = "picD100"
        Me.picD100.Size = New System.Drawing.Size(75, 63)
        Me.picD100.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD100.TabIndex = 30
        Me.picD100.TabStop = False
        '
        'picD101
        '
        Me.picD101.Image = CType(resources.GetObject("picD101.Image"), System.Drawing.Image)
        Me.picD101.Location = New System.Drawing.Point(142, 12)
        Me.picD101.Name = "picD101"
        Me.picD101.Size = New System.Drawing.Size(75, 63)
        Me.picD101.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD101.TabIndex = 31
        Me.picD101.TabStop = False
        '
        'picD102
        '
        Me.picD102.Image = CType(resources.GetObject("picD102.Image"), System.Drawing.Image)
        Me.picD102.Location = New System.Drawing.Point(142, 12)
        Me.picD102.Name = "picD102"
        Me.picD102.Size = New System.Drawing.Size(75, 63)
        Me.picD102.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD102.TabIndex = 32
        Me.picD102.TabStop = False
        '
        'picD103
        '
        Me.picD103.Image = CType(resources.GetObject("picD103.Image"), System.Drawing.Image)
        Me.picD103.Location = New System.Drawing.Point(142, 12)
        Me.picD103.Name = "picD103"
        Me.picD103.Size = New System.Drawing.Size(75, 63)
        Me.picD103.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD103.TabIndex = 33
        Me.picD103.TabStop = False
        '
        'picD104
        '
        Me.picD104.Image = CType(resources.GetObject("picD104.Image"), System.Drawing.Image)
        Me.picD104.Location = New System.Drawing.Point(142, 12)
        Me.picD104.Name = "picD104"
        Me.picD104.Size = New System.Drawing.Size(75, 63)
        Me.picD104.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD104.TabIndex = 34
        Me.picD104.TabStop = False
        '
        'picD105
        '
        Me.picD105.Image = CType(resources.GetObject("picD105.Image"), System.Drawing.Image)
        Me.picD105.Location = New System.Drawing.Point(142, 12)
        Me.picD105.Name = "picD105"
        Me.picD105.Size = New System.Drawing.Size(75, 63)
        Me.picD105.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD105.TabIndex = 35
        Me.picD105.TabStop = False
        '
        'picD106
        '
        Me.picD106.Image = CType(resources.GetObject("picD106.Image"), System.Drawing.Image)
        Me.picD106.Location = New System.Drawing.Point(142, 12)
        Me.picD106.Name = "picD106"
        Me.picD106.Size = New System.Drawing.Size(75, 63)
        Me.picD106.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD106.TabIndex = 36
        Me.picD106.TabStop = False
        '
        'picD107
        '
        Me.picD107.Image = CType(resources.GetObject("picD107.Image"), System.Drawing.Image)
        Me.picD107.Location = New System.Drawing.Point(142, 12)
        Me.picD107.Name = "picD107"
        Me.picD107.Size = New System.Drawing.Size(75, 63)
        Me.picD107.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD107.TabIndex = 37
        Me.picD107.TabStop = False
        '
        'picD108
        '
        Me.picD108.Image = CType(resources.GetObject("picD108.Image"), System.Drawing.Image)
        Me.picD108.Location = New System.Drawing.Point(142, 12)
        Me.picD108.Name = "picD108"
        Me.picD108.Size = New System.Drawing.Size(75, 63)
        Me.picD108.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD108.TabIndex = 38
        Me.picD108.TabStop = False
        '
        'picD109
        '
        Me.picD109.Image = CType(resources.GetObject("picD109.Image"), System.Drawing.Image)
        Me.picD109.Location = New System.Drawing.Point(142, 12)
        Me.picD109.Name = "picD109"
        Me.picD109.Size = New System.Drawing.Size(75, 63)
        Me.picD109.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD109.TabIndex = 39
        Me.picD109.TabStop = False
        '
        'picD1201
        '
        Me.picD1201.Image = CType(resources.GetObject("picD1201.Image"), System.Drawing.Image)
        Me.picD1201.Location = New System.Drawing.Point(142, 110)
        Me.picD1201.Name = "picD1201"
        Me.picD1201.Size = New System.Drawing.Size(75, 63)
        Me.picD1201.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD1201.TabIndex = 40
        Me.picD1201.TabStop = False
        '
        'picD2020
        '
        Me.picD2020.Image = CType(resources.GetObject("picD2020.Image"), System.Drawing.Image)
        Me.picD2020.Location = New System.Drawing.Point(142, 208)
        Me.picD2020.Name = "picD2020"
        Me.picD2020.Size = New System.Drawing.Size(75, 63)
        Me.picD2020.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picD2020.TabIndex = 41
        Me.picD2020.TabStop = False
        '
        'picPercent1000
        '
        Me.picPercent1000.Image = CType(resources.GetObject("picPercent1000.Image"), System.Drawing.Image)
        Me.picPercent1000.Location = New System.Drawing.Point(262, 12)
        Me.picPercent1000.Name = "picPercent1000"
        Me.picPercent1000.Size = New System.Drawing.Size(75, 63)
        Me.picPercent1000.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent1000.TabIndex = 42
        Me.picPercent1000.TabStop = False
        '
        'picPercent1010
        '
        Me.picPercent1010.Image = CType(resources.GetObject("picPercent1010.Image"), System.Drawing.Image)
        Me.picPercent1010.Location = New System.Drawing.Point(262, 12)
        Me.picPercent1010.Name = "picPercent1010"
        Me.picPercent1010.Size = New System.Drawing.Size(75, 63)
        Me.picPercent1010.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent1010.TabIndex = 43
        Me.picPercent1010.TabStop = False
        '
        'picPercent1020
        '
        Me.picPercent1020.Image = CType(resources.GetObject("picPercent1020.Image"), System.Drawing.Image)
        Me.picPercent1020.Location = New System.Drawing.Point(262, 12)
        Me.picPercent1020.Name = "picPercent1020"
        Me.picPercent1020.Size = New System.Drawing.Size(75, 63)
        Me.picPercent1020.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent1020.TabIndex = 44
        Me.picPercent1020.TabStop = False
        '
        'picPercent1030
        '
        Me.picPercent1030.Image = CType(resources.GetObject("picPercent1030.Image"), System.Drawing.Image)
        Me.picPercent1030.Location = New System.Drawing.Point(262, 12)
        Me.picPercent1030.Name = "picPercent1030"
        Me.picPercent1030.Size = New System.Drawing.Size(75, 63)
        Me.picPercent1030.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent1030.TabIndex = 45
        Me.picPercent1030.TabStop = False
        '
        'picPercent1040
        '
        Me.picPercent1040.Image = CType(resources.GetObject("picPercent1040.Image"), System.Drawing.Image)
        Me.picPercent1040.Location = New System.Drawing.Point(262, 12)
        Me.picPercent1040.Name = "picPercent1040"
        Me.picPercent1040.Size = New System.Drawing.Size(75, 63)
        Me.picPercent1040.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent1040.TabIndex = 46
        Me.picPercent1040.TabStop = False
        '
        'picPercent1050
        '
        Me.picPercent1050.Image = CType(resources.GetObject("picPercent1050.Image"), System.Drawing.Image)
        Me.picPercent1050.Location = New System.Drawing.Point(262, 12)
        Me.picPercent1050.Name = "picPercent1050"
        Me.picPercent1050.Size = New System.Drawing.Size(75, 63)
        Me.picPercent1050.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent1050.TabIndex = 47
        Me.picPercent1050.TabStop = False
        '
        'picPercent1060
        '
        Me.picPercent1060.Image = CType(resources.GetObject("picPercent1060.Image"), System.Drawing.Image)
        Me.picPercent1060.Location = New System.Drawing.Point(262, 12)
        Me.picPercent1060.Name = "picPercent1060"
        Me.picPercent1060.Size = New System.Drawing.Size(75, 63)
        Me.picPercent1060.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent1060.TabIndex = 48
        Me.picPercent1060.TabStop = False
        '
        'picPercent1070
        '
        Me.picPercent1070.Image = CType(resources.GetObject("picPercent1070.Image"), System.Drawing.Image)
        Me.picPercent1070.Location = New System.Drawing.Point(262, 12)
        Me.picPercent1070.Name = "picPercent1070"
        Me.picPercent1070.Size = New System.Drawing.Size(75, 63)
        Me.picPercent1070.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent1070.TabIndex = 49
        Me.picPercent1070.TabStop = False
        '
        'picPercent1080
        '
        Me.picPercent1080.Image = CType(resources.GetObject("picPercent1080.Image"), System.Drawing.Image)
        Me.picPercent1080.Location = New System.Drawing.Point(262, 12)
        Me.picPercent1080.Name = "picPercent1080"
        Me.picPercent1080.Size = New System.Drawing.Size(75, 63)
        Me.picPercent1080.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent1080.TabIndex = 50
        Me.picPercent1080.TabStop = False
        '
        'picPercent1090
        '
        Me.picPercent1090.Image = CType(resources.GetObject("picPercent1090.Image"), System.Drawing.Image)
        Me.picPercent1090.Location = New System.Drawing.Point(262, 12)
        Me.picPercent1090.Name = "picPercent1090"
        Me.picPercent1090.Size = New System.Drawing.Size(75, 63)
        Me.picPercent1090.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent1090.TabIndex = 51
        Me.picPercent1090.TabStop = False
        '
        'picPercent10
        '
        Me.picPercent10.Image = CType(resources.GetObject("picPercent10.Image"), System.Drawing.Image)
        Me.picPercent10.Location = New System.Drawing.Point(343, 12)
        Me.picPercent10.Name = "picPercent10"
        Me.picPercent10.Size = New System.Drawing.Size(75, 63)
        Me.picPercent10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent10.TabIndex = 52
        Me.picPercent10.TabStop = False
        '
        'picPercent11
        '
        Me.picPercent11.Image = CType(resources.GetObject("picPercent11.Image"), System.Drawing.Image)
        Me.picPercent11.Location = New System.Drawing.Point(343, 12)
        Me.picPercent11.Name = "picPercent11"
        Me.picPercent11.Size = New System.Drawing.Size(75, 63)
        Me.picPercent11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent11.TabIndex = 53
        Me.picPercent11.TabStop = False
        '
        'picPercent12
        '
        Me.picPercent12.Image = CType(resources.GetObject("picPercent12.Image"), System.Drawing.Image)
        Me.picPercent12.Location = New System.Drawing.Point(343, 12)
        Me.picPercent12.Name = "picPercent12"
        Me.picPercent12.Size = New System.Drawing.Size(75, 63)
        Me.picPercent12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent12.TabIndex = 54
        Me.picPercent12.TabStop = False
        '
        'picPercent13
        '
        Me.picPercent13.Image = CType(resources.GetObject("picPercent13.Image"), System.Drawing.Image)
        Me.picPercent13.Location = New System.Drawing.Point(343, 12)
        Me.picPercent13.Name = "picPercent13"
        Me.picPercent13.Size = New System.Drawing.Size(75, 63)
        Me.picPercent13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent13.TabIndex = 55
        Me.picPercent13.TabStop = False
        '
        'picPercent14
        '
        Me.picPercent14.Image = CType(resources.GetObject("picPercent14.Image"), System.Drawing.Image)
        Me.picPercent14.Location = New System.Drawing.Point(343, 12)
        Me.picPercent14.Name = "picPercent14"
        Me.picPercent14.Size = New System.Drawing.Size(75, 63)
        Me.picPercent14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent14.TabIndex = 56
        Me.picPercent14.TabStop = False
        '
        'picPercent15
        '
        Me.picPercent15.Image = CType(resources.GetObject("picPercent15.Image"), System.Drawing.Image)
        Me.picPercent15.Location = New System.Drawing.Point(343, 12)
        Me.picPercent15.Name = "picPercent15"
        Me.picPercent15.Size = New System.Drawing.Size(75, 63)
        Me.picPercent15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent15.TabIndex = 57
        Me.picPercent15.TabStop = False
        '
        'picPercent16
        '
        Me.picPercent16.Image = CType(resources.GetObject("picPercent16.Image"), System.Drawing.Image)
        Me.picPercent16.Location = New System.Drawing.Point(343, 12)
        Me.picPercent16.Name = "picPercent16"
        Me.picPercent16.Size = New System.Drawing.Size(75, 63)
        Me.picPercent16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent16.TabIndex = 58
        Me.picPercent16.TabStop = False
        '
        'picPercent17
        '
        Me.picPercent17.Image = CType(resources.GetObject("picPercent17.Image"), System.Drawing.Image)
        Me.picPercent17.Location = New System.Drawing.Point(343, 12)
        Me.picPercent17.Name = "picPercent17"
        Me.picPercent17.Size = New System.Drawing.Size(75, 63)
        Me.picPercent17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent17.TabIndex = 59
        Me.picPercent17.TabStop = False
        '
        'picPerecent18
        '
        Me.picPerecent18.Image = CType(resources.GetObject("picPerecent18.Image"), System.Drawing.Image)
        Me.picPerecent18.Location = New System.Drawing.Point(343, 12)
        Me.picPerecent18.Name = "picPerecent18"
        Me.picPerecent18.Size = New System.Drawing.Size(75, 63)
        Me.picPerecent18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPerecent18.TabIndex = 60
        Me.picPerecent18.TabStop = False
        '
        'picPercent19
        '
        Me.picPercent19.Image = CType(resources.GetObject("picPercent19.Image"), System.Drawing.Image)
        Me.picPercent19.Location = New System.Drawing.Point(343, 12)
        Me.picPercent19.Name = "picPercent19"
        Me.picPercent19.Size = New System.Drawing.Size(75, 63)
        Me.picPercent19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picPercent19.TabIndex = 61
        Me.picPercent19.TabStop = False
        '
        'txtCalcAlgorithm
        '
        Me.txtCalcAlgorithm.Location = New System.Drawing.Point(446, 12)
        Me.txtCalcAlgorithm.Name = "txtCalcAlgorithm"
        Me.txtCalcAlgorithm.Size = New System.Drawing.Size(250, 20)
        Me.txtCalcAlgorithm.TabIndex = 62
        '
        'btnCustomCalc
        '
        Me.btnCustomCalc.Location = New System.Drawing.Point(591, 38)
        Me.btnCustomCalc.Name = "btnCustomCalc"
        Me.btnCustomCalc.Size = New System.Drawing.Size(105, 23)
        Me.btnCustomCalc.TabIndex = 63
        Me.btnCustomCalc.Text = "Custom Calculation"
        Me.btnCustomCalc.UseVisualStyleBackColor = True
        '
        'frmDiceCalc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(708, 308)
        Me.Controls.Add(Me.btnCustomCalc)
        Me.Controls.Add(Me.txtCalcAlgorithm)
        Me.Controls.Add(Me.picPercent19)
        Me.Controls.Add(Me.picPerecent18)
        Me.Controls.Add(Me.picPercent17)
        Me.Controls.Add(Me.picPercent16)
        Me.Controls.Add(Me.picPercent15)
        Me.Controls.Add(Me.picPercent14)
        Me.Controls.Add(Me.picPercent13)
        Me.Controls.Add(Me.picPercent12)
        Me.Controls.Add(Me.picPercent11)
        Me.Controls.Add(Me.picPercent10)
        Me.Controls.Add(Me.picPercent1090)
        Me.Controls.Add(Me.picPercent1080)
        Me.Controls.Add(Me.picPercent1070)
        Me.Controls.Add(Me.picPercent1060)
        Me.Controls.Add(Me.picPercent1050)
        Me.Controls.Add(Me.picPercent1040)
        Me.Controls.Add(Me.picPercent1030)
        Me.Controls.Add(Me.picPercent1020)
        Me.Controls.Add(Me.picPercent1010)
        Me.Controls.Add(Me.picPercent1000)
        Me.Controls.Add(Me.picD2020)
        Me.Controls.Add(Me.picD1201)
        Me.Controls.Add(Me.picD109)
        Me.Controls.Add(Me.picD108)
        Me.Controls.Add(Me.picD107)
        Me.Controls.Add(Me.picD106)
        Me.Controls.Add(Me.picD105)
        Me.Controls.Add(Me.picD104)
        Me.Controls.Add(Me.picD103)
        Me.Controls.Add(Me.picD102)
        Me.Controls.Add(Me.picD101)
        Me.Controls.Add(Me.picD100)
        Me.Controls.Add(Me.picD88)
        Me.Controls.Add(Me.picD87)
        Me.Controls.Add(Me.picD86)
        Me.Controls.Add(Me.picD85)
        Me.Controls.Add(Me.picD84)
        Me.Controls.Add(Me.picD83)
        Me.Controls.Add(Me.picD82)
        Me.Controls.Add(Me.picD81)
        Me.Controls.Add(Me.picD66)
        Me.Controls.Add(Me.picD65)
        Me.Controls.Add(Me.picD64)
        Me.Controls.Add(Me.picD63)
        Me.Controls.Add(Me.picD62)
        Me.Controls.Add(Me.picD61)
        Me.Controls.Add(Me.picD44)
        Me.Controls.Add(Me.picD43)
        Me.Controls.Add(Me.picD42)
        Me.Controls.Add(Me.picD41)
        Me.Controls.Add(Me.lblResult)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblResultLog)
        Me.Controls.Add(Me.rtbDebug)
        Me.Controls.Add(Me.btnRollPercent)
        Me.Controls.Add(Me.btnRollD20)
        Me.Controls.Add(Me.btnRollD12)
        Me.Controls.Add(Me.btnRollD10)
        Me.Controls.Add(Me.btnRollD8)
        Me.Controls.Add(Me.btnRollD6)
        Me.Controls.Add(Me.btnRollD4)
        Me.Name = "frmDiceCalc"
        Me.Text = "Dice Roll Calculator"
        CType(Me.picD41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD102, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD105, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD106, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD108, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD109, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD1201, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picD2020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent1000, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent1010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent1020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent1030, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent1040, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent1050, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent1060, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent1070, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent1080, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent1090, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPerecent18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPercent19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnRollD4 As Button
    Friend WithEvents btnRollD6 As Button
    Friend WithEvents btnRollD8 As Button
    Friend WithEvents btnRollD10 As Button
    Friend WithEvents btnRollD12 As Button
    Friend WithEvents btnRollD20 As Button
    Friend WithEvents btnRollPercent As Button
    Friend WithEvents rtbDebug As RichTextBox
    Friend WithEvents lblResultLog As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblResult As Label
    Friend WithEvents picD41 As PictureBox
    Friend WithEvents picD42 As PictureBox
    Friend WithEvents picD43 As PictureBox
    Friend WithEvents picD44 As PictureBox
    Friend WithEvents picD61 As PictureBox
    Friend WithEvents picD62 As PictureBox
    Friend WithEvents picD63 As PictureBox
    Friend WithEvents picD64 As PictureBox
    Friend WithEvents picD65 As PictureBox
    Friend WithEvents picD66 As PictureBox
    Friend WithEvents picD81 As PictureBox
    Friend WithEvents picD82 As PictureBox
    Friend WithEvents picD83 As PictureBox
    Friend WithEvents picD84 As PictureBox
    Friend WithEvents picD85 As PictureBox
    Friend WithEvents picD86 As PictureBox
    Friend WithEvents picD87 As PictureBox
    Friend WithEvents picD88 As PictureBox
    Friend WithEvents picD100 As PictureBox
    Friend WithEvents picD101 As PictureBox
    Friend WithEvents picD102 As PictureBox
    Friend WithEvents picD103 As PictureBox
    Friend WithEvents picD104 As PictureBox
    Friend WithEvents picD105 As PictureBox
    Friend WithEvents picD106 As PictureBox
    Friend WithEvents picD107 As PictureBox
    Friend WithEvents picD108 As PictureBox
    Friend WithEvents picD109 As PictureBox
    Friend WithEvents picD1201 As PictureBox
    Friend WithEvents picD2020 As PictureBox
    Friend WithEvents picPercent1000 As PictureBox
    Friend WithEvents picPercent1010 As PictureBox
    Friend WithEvents picPercent1020 As PictureBox
    Friend WithEvents picPercent1030 As PictureBox
    Friend WithEvents picPercent1040 As PictureBox
    Friend WithEvents picPercent1050 As PictureBox
    Friend WithEvents picPercent1060 As PictureBox
    Friend WithEvents picPercent1070 As PictureBox
    Friend WithEvents picPercent1080 As PictureBox
    Friend WithEvents picPercent1090 As PictureBox
    Friend WithEvents picPercent10 As PictureBox
    Friend WithEvents picPercent11 As PictureBox
    Friend WithEvents picPercent12 As PictureBox
    Friend WithEvents picPercent13 As PictureBox
    Friend WithEvents picPercent14 As PictureBox
    Friend WithEvents picPercent15 As PictureBox
    Friend WithEvents picPercent16 As PictureBox
    Friend WithEvents picPercent17 As PictureBox
    Friend WithEvents picPerecent18 As PictureBox
    Friend WithEvents picPercent19 As PictureBox
    Friend WithEvents txtCalcAlgorithm As TextBox
    Friend WithEvents btnCustomCalc As Button
End Class
